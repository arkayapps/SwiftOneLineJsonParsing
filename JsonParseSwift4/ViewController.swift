//
//  ViewController.swift
//  JsonParseSwift4
//
//  Created by Brian Voong on 6/30/17.
//  Copyright © 2017 Lets Build That App. All rights reserved.
//

import UIKit

struct Level: Decodable {
    let lelveno: String?
    let levelname: String?
}
class ViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var heptorTableView: UITableView!
    
    var levels:[Level] = []
     let cellReuseIdentifier = "cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        heptorTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        heptorTableView.delegate = self
        heptorTableView.dataSource = self
        
        let jsonUrlString = "http://arkayapps.com/quizpower/getleveliinfo.php"
        guard let url = URL(string: jsonUrlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            //perhaps check err
            //also perhaps check response status 200 OK
            
            guard let data = data else { return }
            
            do {
                
                self.levels = try JSONDecoder().decode([Level].self, from: data)
                DispatchQueue.main.async {
                    self.heptorTableView.reloadData()
                }

                
            } catch let jsonErr {
                print("Error serializing json:", jsonErr)
            }
            
                        
            
        }.resume()

    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Count: \(self.levels.count)")
        return self.levels.count
        
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = heptorTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell!

        var someVar = self.levels[indexPath.row]
        var temp1 : String! // This is not optional.
        temp1 = (someVar.levelname)
        cell.textLabel?.text =  temp1
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        //selectedIndex = indexPath.row;
        //self.performSegue(withIdentifier: "SubjecToCheptorSegue", sender: self)
    }
    
    // this method handles row deletion
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        print(indexPath.row);
        
       
    }
  

}









